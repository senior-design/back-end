import os
import uuid
# import orjson
import asyncio
import json
import logging

# from server import Server
# from server.helpers.status_codes import HTTP_NO_CONTENT

from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaRecorder

from aiohttp import web
from ssl import SSLContext

pcs = set()

CERT_PATH = os.environ['CERT_PATH']
KEY_PATH = os.environ['KEY_PATH']

# app = Server(useSSL=True, cert=(CERT_PATH, KEY_PATH))
# app = Server(port=8080)

# @app.use
# def cors(request, response):
#     if request.method == "OPTIONS":
#         if "Access-Control-Request-Method" in request.headers:
#             response.addHeader(("Access-Control-Allow-Methods", "GET, POST"))
#         if "Access-Control-Request-Headers" in request.headers:
#             response.addHeader(("Access-Control-Allow-Headers", "Content-Type"))
#         if "Origin" in request.headers:
#             response.addHeader(("Access-Control-Allow-Origin", request.headers["Origin"]))
#         response.status_code = HTTP_NO_CONTENT

ROOT = os.path.dirname(__file__)


async def index(request):
    content = open(os.path.join(ROOT, "index.html"), "r").read()
    return web.Response(content_type="text/html", text=content)


async def javascript(request):
    content = open(os.path.join(ROOT, "index.js"), "r").read()
    return web.Response(content_type="application/javascript", text=content)


async def css(request):
    content = open(os.path.join(ROOT, "index.css"), "r").read()
    return web.Response(content_type="text/css", text=content)


async def test(request):
    return web.Response(
        content_type="application/json",
        text=json.dumps({"name": "Prasenjit Gaurav", "test": "Working"})
    )


# @app.route('/offer', 'POST')
# async def offer(request, response):
async def offer(request):
    print("Request recieved")
    # params = orjson.loads(request.body)
    params = await request.json()
    incoming = RTCSessionDescription(sdp=params["sdp"], type=params["type"])

    pc = RTCPeerConnection()
    pc_id = "PeerConnection(%s)" % uuid.uuid4()
    pcs.add(pc)

    def log_info(msg, *args):
        logger.info(pc_id + " " + msg, *args)

    # log_info("Created for %s", request.url.path.decode())
    log_info("Created for %s", request.remote)

    recorder = MediaRecorder(os.path.join(os.getcwd(), "test.mp4"))

    @pc.on("iceconnectionstatechange")
    async def on_iceconnectionstatechange():
        log_info("ICE connection state is %s", pc.iceConnectionState)
        print("ICE Connection State:", pc.iceConnectionState)
        if pc.iceConnectionState == "failed":
            await pc.close()
            pcs.discard(pc)

    @pc.on("track")
    def on_track(track):
        log_info("Track %s received", track.kind)
        recorder.addTrack(track)

        @track.on("ended")
        async def on_ended():
            log_info("Track %s ended", track.kind)
            await recorder.stop()

    # handle offer
    await pc.setRemoteDescription(incoming)
    await recorder.start()

    # send answer
    answer = await pc.createAnswer()
    await pc.setLocalDescription(answer)

    # response.addHeader(('Content-Type', 'application/json'))
    # response.body = orjson.dumps({"sdp": pc.localDescription.sdp, "type": pc.localDescription.type})

    return web.Response(
        content_type="application/json",
        text=json.dumps(
            {
                "sdp": pc.localDescription.sdp,
                "type": pc.localDescription.type
            }
        )
    )


async def on_shutdown(app):
    # close peer connections
    coros = [pc.close() for pc in pcs]
    await asyncio.gather(*coros)
    pcs.clear()


# app.serve()
if __name__ == "__main__":
    sslContext = SSLContext()
    sslContext.load_cert_chain(CERT_PATH, KEY_PATH)

    logger = logging.getLogger("pc")
    f_handler = logging.FileHandler('myapp.log', 'a')
    f_handler.setLevel(logging.DEBUG)
    logger.addHandler(f_handler)

    app = web.Application()
    app.on_shutdown.append(on_shutdown)
    app.router.add_get("/", index)
    app.router.add_get("/index.js", javascript)
    app.router.add_get("/index.css", css)
    app.router.add_post("/offer", offer)
    app.router.add_get("/test", test)
    web.run_app(
        app, access_log=logger, host="localhost", port=443, ssl_context=sslContext
    )
