var pc = null;

const createPeerConnection = function()
{
    let config = {
        sdpSemantics: 'unified-plan',
        // iceServers: [{urls: ['stun:stun.l.google.com:19302', 'stun:stun2.l.google.com:19302']}]
    };
    pc = new RTCPeerConnection(config);

    // connect audio / video
    pc.addEventListener('track', function(evt) {
        if (evt.track.kind === 'video')
            document.getElementById('video').srcObject = evt.streams[0];
        else
            document.getElementById('audio').srcObject = evt.streams[0];
    });
    return pc;
}

const negotiate = function ()
{
    return pc.createOffer()
    .then(function (offer) {
        return pc.setLocalDescription(offer);
    })
    .then(function () {
    // wait for ICE gathering to complete
        return new Promise(function(resolve) {
            if (pc.iceGatheringState === 'complete') {
                resolve();
            } else {
                function checkState() {
                    if (pc.iceGatheringState === 'complete') {
                        pc.removeEventListener('icegatheringstatechange', checkState);
                        resolve();
                    }
                }
                pc.addEventListener('icegatheringstatechange', checkState);
            }
        });
    })
    .then(function () {
        let offer = pc.localDescription;
        return fetch('https://localhost/offer', {
            body: JSON.stringify({
                sdp: offer.sdp,
                type: offer.type,
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            mode: 'no-cors'
        });
    })
    .then(function (response) {
        return response.json()
    })
    .then(function (answer) {
        console.log(answer)
        return pc.setRemoteDescription(answer);
    })
    .catch(function (e) {
        console.log(e)
        alert(e);
    });
}

const start = function()
{
    document.getElementById('start').style.display = 'none';
    pc = createPeerConnection();
    let constraints = {
        audio: true,
        video: {
                width: 640,
                height: 480
            }
    };
    document.getElementById('media').style.display = 'block';
    navigator.mediaDevices.getUserMedia(constraints)
    .then(
        function (stream) {
            stream.getTracks()
                .forEach(function (track) {
                    pc.addTrack(track, stream);
                });
            return negotiate();
        },
        function (err) {
            alert('Could not acquire media: ' + err);
        }
    );
    document.getElementById('stop').style.display = 'inline-block';
}

const stop = function()
{
    document.getElementById('stop').style.display = 'none';
    // close transceivers
    if (pc.getTransceivers) {
        pc.getTransceivers().forEach(function(transceiver) {
            if (transceiver.stop) {
                transceiver.stop();
            }
        });
    }
    // close local audio / video
    pc.getSenders().forEach(function(sender) {
        sender.track.stop();
    });
    // close peer connection
    setTimeout(function() {
        pc.close();
    }, 500);
}