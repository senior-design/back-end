import os
from server import Server

_RESP_CACHE = {}

CERT_PATH = os.environ['CERT_PATH']
KEY_PATH = os.environ['KEY_PATH']
app = Server(useSSL=True, cert=(CERT_PATH, KEY_PATH))


@app.route('/offer')
def id_handler(req, res):
    payload_size = 100
    resp = b'X' * payload_size
    res.body = resp


app.serve()
