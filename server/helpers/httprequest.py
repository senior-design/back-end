class HttpRequest:
    __slots__ = ('__url', '__body', '__headers', '__method', '__protocol')

    def __init__(self, url=None, body=None):
        self.__url = url
        self.__body = body
        self.__method = None
        self.__headers = {}
        self.__protocol = None

    @property
    def headers(self):
        return self.__headers

    @headers.setter
    def headers(self, h_list):
        for name, value in h_list:
            self.__headers[name] = value

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, val):
        if self.__url is None:
            self.__url = val
        else:
            raise ValueError('URL can be set only once.')

    @property
    def body(self):
        return self.__body

    def __write_raw_body(self, data):
        if not isinstance(data, bytes):
            raise TypeError(f"\'bytes\' data type expected. Provided \'{type(data)}\'")
        if self.__body is None:
            self.__body = data
        else:
            self.__body += data

    @property
    def method(self):
        return self.__method

    @method.setter
    def method(self, val):
        if self.__method is None:
            if isinstance(val, bytes):
                self.__method = val.decode().upper()
            else:
                self.__method = val.upper()
        else:
            raise ValueError('Method cannot be overwritten')

    @property
    def protocol(self):
        return self.__protocol

    @protocol.setter
    def protocol(self, val):
        if self.__protocol is None:
            self.__protocol = val
        else:
            raise ValueError('Protocol cannot be overwritten')