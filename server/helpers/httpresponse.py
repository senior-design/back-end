class HttpResponse:
    __slots__ = ('__protocol', '__request', '__headers'
                 , '_transport', '__body', '__status_code')

    def __init__(self, request, transport):
        self.__protocol = request.protocol
        self.__request = request
        self.__headers = []
        self._transport = transport
        self.__body = None
        self.__status_code = "200 OK"

    @property
    def protocol(self):
        return self.__protocol

    @property
    def status_code(self):
        return self.__status_code

    @status_code.setter
    def status_code(self, val):
        self.__status_code = val

    @property
    def body(self):
        return self.__body

    @body.setter
    def body(self, data):
        if isinstance(data, bytes):
            self.__body = data
        else:
            self.__body = data.encode()

    @property
    def headers(self):
        return self.__headers

    @headers.setter
    def headers(self, headers):
        self.__headers = headers

    def write(self, data):
        if not isinstance(data, bytes):
            raise TypeError(f"\'bytes\' data type expected. Provided \'{type(data)}\'")
        if self.__body:
            self.__body += data
        else:
            self.__body = data

    async def send(self, header='DEFAULT_HEADER'):
        if header is 'DEFAULT_HEADER':
            headers = b''
            if self.headers:
                for i in self.headers:
                    headers += f'{i[0]}: {i[1]}'.encode()
        else:
            headers = b''
            for i in header:
                headers += f'{i[0]}: {i[1]}'.encode()
        if self.body:
            headers += f'Content-Length: {len(self.body)}\r\n'.encode()
            data = b''.join([
                f'HTTP/1.1 {self.status_code}\r\n'.encode(),
                headers,
                b'\r\n',
                self.body,
            ])
        else:
            data = b''.join([
                f'HTTP/1.1 {self.status_code}\r\n'.encode(),
                headers,
                b'\r\n'
            ])
        self._transport.write(data)
        await self._transport.drain()

    def addHeader(self, header):
        self.__headers.append(header)