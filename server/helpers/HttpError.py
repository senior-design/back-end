__all__ = ('HttpError', 'ProtocolError')


class HttpError(Exception):
    def __init__(self, code, message=None):
        self.status_code = code
        self.message = message or code


class ProtocolError(Exception):
    def __init__(self, message=None):
        self.message = message
