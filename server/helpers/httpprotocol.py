from socket import IPPROTO_TCP, TCP_NODELAY

from httptools import parse_url


class HttpProtocol:
    PROCESSING, CONTINUE_NEEDED, COMPLETE = 0, 1, 2
    __slots__ = ('_current_request', '_current_headers', '_current_state')

    def __init__(self, request):
        self._current_request = request
        self._current_headers = []
        self._current_state = self.PROCESSING

    def on_message_begin(self):
        pass

    def on_chunk_header(self):
        pass

    def on_chunk_complete(self):
        pass

    def on_url(self, url):
        self._current_request.url = parse_url(url)

    def on_header(self, name, value):
        self._current_headers.append((name.decode(), value.decode()))
        if name.lower() == b'expect' and value == b'100-continue':
            self._current_state = self.CONTINUE_NEEDED

    def on_headers_complete(self):
        self._current_request.headers = self._current_headers

    def on_body(self, body):
        self._current_request._HttpRequest__write_raw_body(body)

    def on_status(self, status):
        print(status.decode('utf-8'))

    def on_message_complete(self):
        self._current_state = self.COMPLETE

    def connection_made(self, transport):
        sock = transport.get_extra_info('socket')
        try:
            sock.setsockopt(IPPROTO_TCP, TCP_NODELAY, 1)
        except (OSError, NameError):
            pass

    def connection_lost(self):
        pass

    @property
    def finished(self):
        return self._current_state == self.COMPLETE

    @property
    def needs_write_continue(self):
        return self._current_state == self.CONTINUE_NEEDED

    def reset_state(self):
        self._current_state = self.PROCESSING
