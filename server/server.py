__all__ = ['Server']

import asyncio
import os
import ssl
import httptools

from server.helpers import HttpProtocol
from server.helpers import Request, Response, ProtocolError, HTTP_404, HTTP_CONTINUE


class Router:
    __slots__ = ('__middlewareStack', '_urls', '__error_handler')

    def __init__(self):
        self.__middlewareStack = []
        self._urls = {
            'GET': {},
            'POST': {},
            'PUT': {},
            'DELETE': {},
            'HEAD': {}
        }
        self.__error_handler = self.handle_error

    @property
    def middlewares(self):
        return self.__middlewareStack

    @property
    def routes(self):
        return self._urls

    @property
    def errorhandler(self):
        return self.__error_handler

    @errorhandler.setter
    def errorhandler(self, func):
        if func.__code__.co_argcount != 3:
            raise AssertionError(f"Invalid number of arguments. Required 3, Given {func.__code__.co_argcount}")
        self.__error_handler = func

    def use(self, func):
        if func.__code__.co_argcount != 2:
            raise AssertionError("Incorrect number of parameters. Middlewares have signature Foo(req, res).")
        self.__middlewareStack.append(func)

    def route(self, path, method='GET'):
        rules = self._urls

        def __add_url_rule(func):
            if func.__code__.co_argcount != 2:
                raise AssertionError(
                    f"Not correct number of parameters. Need: 2,   Provided: {func.__code__.co_argcount}\n "
                    f"Middlewares have signature Foo(req, res).")
            rules[method.upper()][path] = func

        return __add_url_rule

    async def _process(self, req, res):
        for i in self.middlewares:
            i(req, res)

        method = req.method
        path = req.url.path.decode()
        if path in self.routes[method]:
            if asyncio.iscoroutinefunction(self.routes[method][path]):
                await self.routes[method][path](req, res)
            else:
                self.routes[method][path](req, res)
        else:
            raise NotImplementedError(f"{HTTP_404}")

    def handle_error(self, req, res, error):
        res.status_code = HTTP_404
        res.body = ''.join(error.args)
        return None

    def print_routes(self):
        print(self._urls)


class Server(Router):
    def __init__(self, host="0.0.0.0", port=443, useSSL=False, cert=None):
        super().__init__()
        if useSSL:
            if cert and len(cert) == 2:
                cert_path, key_path = cert
                self.__sslContext = self.create_sslcontext(cert_path, key_path)
                self.__ssl = True
            else:
                self.__ssl = False
                raise ssl.SSLError("SSL Context missing.")
        else:
            self.__ssl = False
        try:
            import uvloop
            self.__loop = uvloop.new_event_loop()
            asyncio.set_event_loop(self.__loop)
        except ImportError:
            self.__loop = asyncio.get_event_loop()

        self.__loop.set_debug(False)
        self.host = host
        self.port = port
        self.address = (self.host, self.port)

    async def __receive(self, streamReader, streamWriter):
        length = 65536
        req = Request()
        protocol = HttpProtocol(req)
        parser = httptools.HttpRequestParser(protocol)
        while True:
            data = await streamReader.read(length)
            parser.feed_data(data)
            if protocol.finished or not data:
                break
            elif protocol.needs_write_continue:
                await streamWriter.write(
                    streamWriter,
                    f'HTTP/{parser.get_http_version()} {HTTP_CONTINUE}\r\n'.encode()
                )
                protocol.reset_state()
        if req.url is None:
            return
        req.protocol = parser.get_http_version()
        req.method = parser.get_method()
        res = Response(req, streamWriter)
        try:
            await self._process(req, res)
            await res.send()
            if not parser.should_keep_alive():
                streamWriter.close()
        except Exception as e:
            print('Error:', e)
            headers = self.errorhandler(req, res, e)
            if headers:
                await res.send(headers)
            else:
                await res.send()
            streamWriter.close()

    def serve(self):
        print(f'Starting the server on {self.host}:{self.port}')
        self.__loop.create_task(
            asyncio.start_server(
                self.__receive,
                host=self.host,
                port=self.port,
                ssl=self.__sslContext if self.__ssl else None
            )
        )
        try:
            self.__loop.run_forever()
        finally:
            self.__loop.close()

    def create_sslcontext(self, cert_path, key_path):
        if not os.path.exists(cert_path):
            raise ProtocolError("SSL Certificate not found. Please provide the path to the certificate.")
        if not os.path.exists(key_path):
            raise ProtocolError("SSL key not found. Please provide the path to the certificate.")
        sslContext = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        sslContext.load_cert_chain(certfile=cert_path, keyfile=key_path)
        sslContext.options |= (
                ssl.OP_NO_SSLv2 |
                ssl.OP_NO_SSLv3 |
                ssl.OP_NO_TLSv1 |
                ssl.OP_NO_TLSv1_1
        )
        sslContext.verify_mode = ssl.VerifyMode.CERT_NONE
        sslContext.set_alpn_protocols(['http/1.1'])
        try:
            sslContext.set_npn_protocols(['http/1.1'])
        except NotImplementedError:
            pass
        return sslContext
